FROM alpine:3.18.4 as builder

ARG TERRAFORM_VERSION=1.6.4
ARG SCALEWAY_CLI_VERSION=2.24.0
ARG KUBECTL_VERSION=1.28.4
ARG HELM_VERSION=3.13.2

RUN apk add curl --no-cache

RUN curl -fsSL https://releases.hashicorp.com/terraform/${TERRAFORM_VERSION}/terraform_${TERRAFORM_VERSION}_linux_amd64.zip -o terraform.zip \
    && unzip terraform.zip -d /usr/local/bin \
    && rm terraform.zip

RUN curl -sL https://github.com/scaleway/scaleway-cli/releases/download/v${SCALEWAY_CLI_VERSION}/scaleway-cli_${SCALEWAY_CLI_VERSION}_linux_amd64 -o /usr/local/bin/scw \
    && chmod +x /usr/local/bin/scw

RUN curl -LO "https://storage.googleapis.com/kubernetes-release/release/v${KUBECTL_VERSION}/bin/linux/amd64/kubectl" \
    && chmod +x kubectl \
    && mv kubectl /usr/local/bin/kubectl

RUN curl -fsSL https://get.helm.sh/helm-v${HELM_VERSION}-linux-amd64.tar.gz | tar xz \
    && mv linux-amd64/helm /usr/local/bin/helm

FROM alpine:3.18.4

ARG ANSIBLE_VERSION=9.0.1

RUN apk add --no-cache bash python3 py3-pip \
    && pip3 install --upgrade pip \
    && pip3 install ansible==${ANSIBLE_VERSION}

COPY --from=builder /usr/local/bin/terraform /usr/local/bin/terraform
COPY --from=builder /usr/local/bin/scw /usr/local/bin/scw
COPY --from=builder /usr/local/bin/kubectl /usr/local/bin/kubectl
COPY --from=builder /usr/local/bin/helm /usr/local/bin/helm

RUN rm -rf /var/cache/apk/* \
    && adduser -D hunty

USER hunty
WORKDIR /home/hunty

CMD ["tail", "-f", "/dev/null"]
