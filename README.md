`devops-toolkit` is a Docker image containing a suite of essential DevOps tools including Terraform, Ansible, Scaleway CLI, kubectl, and Helm. This image is designed to provide a consistent and isolated environment for managing cloud infrastructure and deployments. The repository also includes a script for convenient execution of these tools from the host machine.

## Included tools and versions

This image includes the following tools with the specified versions:

- Terraform: 1.6.4
- Ansible: 9.0.1
- Scaleway CLI: 2.24.0
- kubectl: 1.28.4
- Helm: 3.13.2

## Usage

The `devops-toolkit` image is available on a public registry. You can pull and use this image without needing to build it locally.

### Pulling the image

To pull the `devops-toolkit` image from the public registry, run:

```bash
docker pull registry.gitlab.com/wearehunty/devops-toolkit:latest
```

### Using aliases

1. Add the following line to your `.bash_aliases` depending on your needs:

    ```bash
    alias terraform='docker exec -it devops-toolkit terraform'
    alias ansible='docker exec -it devops-toolkit ansible'
    alias ansible-playbook='docker exec -it devops-toolkit ansible-playbook'
    alias scw='docker exec -it devops-toolkit scw'
    alias kubectl='docker exec -it devops-toolkit kubectl'
    alias helm='docker exec -it devops-toolkit helm'
    ```

2. Reload your shell configuration

    ```bash
    source ~/.bash_aliases
    ```

3. Run the container in detached mode

    ```bash
    docker run -d --name devops-toolkit registry.gitlab.com/wearehunty/devops-toolkit
    ```

4. Use the aliases

    Use the defined aliases such as `terraform`, `ansible`, `scw`, `kubectl`, and `helm` to run the respective tools inside the Docker container.

## Contributing

Contributions to the devops-toolkit are welcome. You can contribute by improving the Dockerfile, updating the tool versions, or enhancing the alias script.

1. Fork the Repository: Create a fork of this repository on your GitLab account.
2. Create a Feature Branch: Make your changes in a new branch in your fork.
3. Submit a Merge Request: After pushing your changes to your fork, submit a merge request for review and potential merge into the main repository.

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details.
